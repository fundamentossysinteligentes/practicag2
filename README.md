# README #
DESARROLLO DE UN SISTEMA EXPERTO UTILIZANDO GENSYM G2
Práctica Obligatoria Fundamentos de Sistemas Inteligentes
Grado en Ingeniería Informática
Marketing online basado en SS.EE. para un plataforma de reservas hoteleras
Las empresas de marketing online especializadas en el tratamiento comercial de reservas de
hoteles implementan algoritmos muy complejos para la adecuación de los precios y las ofertas de los
hoteles ante una creciente globalización del negocio.
Estos algoritmos están basados en la gestión de ingentes cantidades de datos, que mezclan
diversos aspectos de la Inteligencia Artificial, Big Data, Data mining, etc.
Parte del desarrollo de estos algoritmos tienen una base muy sustentada: los sistemas expertos.
En esta práctica se pretende adentrar muy sutilmente (y muy básicamente) en estos sistemas.
Para ello, se va a implementar un algoritmo de recomendación y marketing muy básico, pero en
donde seguramente muchos de los aspectos que se destacan, efectivamente sean ciertos en el mundo
actual.
El algoritmo se desarrolla para una plataforma de reservas hoteleras. Comienza a funcionar
monitorizando las visitas realizadas por un usuario en la plataforma.
Si durante los últimos 10 minutos de navegación el usuario ha visitado menos de 3 hoteles, eso
significará que el usuario tiene bastante definida la demanda, por lo que el algoritmo de marketing
habrá de buscar el beneficio mediante mejoras en la calidad de los servicios contratados. Es por ello
que se sugerirá al usuario un hotel de categoría superior a la visitada (por ejemplo, si se encuentra en un
hotel de 3 estrellas, se sugerirá un hotel de 4 estrellas). En caso de que el usuario visite dicho hotel
denotará un interés por la calidad del servicio (y no tanto por el precio ofertado). Por ello, dentro de las
habitaciones de dicho hotel, se ofertarán inicialmente las habitaciones de nivel superior (habitación
executive, junior suites, suites) para dejar para el final las habitaciones más económicas. Si el usuario
visita una de esas habitaciones superiores, el algoritmo buscará cerrar rápidamente la reserva ofertando
un 15% de descuento sobre el precio original de la habitación. En caso de que no se visite una
habitación superior, sino que se visite una habitación económica, se preguntará al usuario por la
posibilidad de añadir el desayuno en la reserva. Si el usuario lo solicita afirmativamente, se ofrecerá el
precio estándar de la habitación y un 50% de descuento en el desayuno. Si el usuario contesta
negativamente al desayuno incluido, se ofrecerá un 10% de descuento sobre el precio de la habitación
(buscando así acelerar la decisión de la reserva).
Si el usuario ha visitado 3 ó más hoteles durante los últimos 10 minutos, se monitorizará si el
usuario ha aplicado algún tipo de filtro por precio en la búsqueda. En caso de que se utilizara algún tipo
de filtro por precio, tendremos 3 opciones:
- Que el filtrado sea menor a 50€ por noche (es un usuario que busca pernoctaciones baratas).
Para ello, aplicaremos el 10% de descuento en cualquier oferta que se le proporcione (incentivando al
factor precio en la reserva)
- Que el filtrado esté entre 50€ y 100€ por noche (es un usuario que busca una ratio
precio/servicio óptima). Para ello, siempre se le preguntará por la posibilidad de incorporar el desayuno
a la oferta (estado visto anteriormente) y se continuará con el flujo descrito previamente.
- Que el filtrado esté por encima de los 100€ por noche (es un usuario que busca servicios). Se
incentivarán siempre las habitaciones superiores del hotel (estado visto anteriormente) y se continuará
con el flujo descrito previamente.
En caso negativo, simplemente se priorizarán los hoteles de 4 estrellas. Si el usuario visita uno
de estos hoteles, se sugerirán siempre las habitaciones superiores del hotel (estado visto anteriormente)
y se continuará con el flujo descrito previamente. Si el usuario no visita uno de estos hoteles, se
aplicará siempre un 5% de descuento al precio original de la reserva.
Finalmente, en el caso de que un usuario haya hecho menos de 3 visitas a hoteles en los últimos
10 minutos y haya rechazado la sugerencia de un hotel de superior categoría, siempre se preguntará por
la posibilidad de añadir el desayuno a la reserva (estado visitado anteriormente) y se continuará con el
flujo descrito previamente.
Realizar el sistema lineal y el sistema con metaconocimiento.